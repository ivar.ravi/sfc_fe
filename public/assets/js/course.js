console.log("hello from js");

//The first thing that we need to do is to identify which course it needs to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//window.location. -> returns a location object with information about the "current" location of the document.
// .seach => contains the query string section of the current URL. //QUERY STRING IS AN OBJECT // search property returns an object of type stringString.
//URLSearchParam() -> this method/constructor creates and returns a new URLSearchParams object. (this is a class)
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL. (this is a prop type)
// new -> instantiate a user-defined object.
// "URLSearchParams" -> template for creating the object

// params ={
	//"courseId": "....id ng course that we passed"
//}
let id = params.get('courseId')
console.log(id)

//lets capture the sections of the html body.
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")

fetch(`http://localhost:4000/api/courses/${id}`).then(
	res => res.json()).then(data => {
		console.log(data)

		name.innerHTML = data.name
		desc.innerHTML = data.description
		price.innerHTML = data.price
	})